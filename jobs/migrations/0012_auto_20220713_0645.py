# Generated by Django 3.1.1 on 2022-07-13 06:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0011_auto_20220713_0643'),
    ]

    operations = [
        migrations.RenameField(
            model_name='findtalentrequest',
            old_name='name',
            new_name='first_name',
        ),
        migrations.AddField(
            model_name='findtalentrequest',
            name='last_name',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
