# Generated by Django 3.1.1 on 2022-07-13 06:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0014_auto_20220713_0647'),
    ]

    operations = [
        migrations.RenameField(
            model_name='findtalentrequest',
            old_name='user_email',
            new_name='email',
        ),
    ]
