# Generated by Django 3.1.1 on 2022-07-12 12:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0008_auto_20220712_1117'),
    ]

    operations = [
        migrations.AddField(
            model_name='jobrequest',
            name='terms_of_service',
            field=models.CharField(choices=[('Contract', 'Contract'), ('Temporary', 'Temporary'), ('Permanent', 'Permanent')], max_length=100, null=True),
        ),
        migrations.CreateModel(
            name='FindTalentRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('job_title', models.CharField(max_length=100, null=True)),
                ('location', models.CharField(choices=[('Nairobi', 'Nairobi'), ('Kisumu', 'Kisumu'), ('Mombasa', 'Mombasa'), ('nakuru', 'Nairobi')], max_length=100, null=True)),
                ('contacts', models.CharField(max_length=100, null=True)),
                ('terms_of_service', models.CharField(choices=[('Contract', 'Contract'), ('Temporary', 'Temporary'), ('Parmanent', 'Parmanent')], max_length=100, null=True)),
                ('area_of_specialization', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='jobs.jobpost')),
            ],
        ),
    ]
