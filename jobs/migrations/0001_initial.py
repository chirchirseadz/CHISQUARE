# Generated by Django 3.1.1 on 2022-07-09 19:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='JobPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('area_of_specialization', models.CharField(choices=[('IT specialist', 'IT specialist'), ('Administative Assistant', 'Administrative Assistant'), ('Clerical And Data Entry Personel ', 'Clerical And Data Entry Personel'), ('Hospitality', 'Hospitality'), ('Customer Care', 'Customer Care'), ('Hospitality And Wait Staff', 'Hospitality And Wait Staff')], max_length=100, null=True)),
                ('title', models.CharField(max_length=50)),
                ('job_desc', models.TextField(blank=True)),
                ('budget', models.FloatField()),
                ('date_posted', models.DateTimeField(auto_now_add=True)),
                ('terms_and_conditions', models.BooleanField()),
                ('job_taken', models.BooleanField(default=False)),
                ('approved', models.BooleanField(default=False)),
                ('employer', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='users.user_profile')),
            ],
        ),
        migrations.CreateModel(
            name='jobrequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('proposal', models.TextField(blank=True)),
                ('your_budget', models.FloatField()),
                ('terms_and_conditions', models.BooleanField()),
                ('title', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='jobs.jobpost')),
                ('user_requesting', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='users.user_profile')),
            ],
        ),
    ]
